import { CarderPage } from './app.po';

describe('carder App', () => {
  let page: CarderPage;

  beforeEach(() => {
    page = new CarderPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
