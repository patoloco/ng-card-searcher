import { Component, OnInit } from '@angular/core';
import { Card } from '../../Card';
import { CardSearchService } from '../../card-search/card-search.service';

@Component({
  selector: 'app-tab-text',
  templateUrl: './tab-text.component.html'
})

export class TabTextComponent implements OnInit {
  cardOne: Card;

  constructor(private cardsearchservice: CardSearchService) {
  }

  ngOnInit() {
    this.cardOne = this.cardsearchservice.getCurrentCard();
  }

}
