import { Component, OnInit } from '@angular/core';

import { CardSearchService } from '../../card-search/card-search.service';

import { Card } from '../../Card';

@Component({
  selector: 'app-tab-types',
  templateUrl: './tab-types.component.html'
})
export class TabTypesComponent implements OnInit {
  cardOne: Card;

  constructor(private search: CardSearchService) {
    this.cardOne = this.search.getCurrentCard();
    this.search.cardUpdated.subscribe((card: Card) => {
      this.cardOne = card;
    });
  }

  ngOnInit() {
    this.cardOne = this.search.getCurrentCard();
  }

}
