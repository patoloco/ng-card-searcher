import { Component, Input} from '@angular/core';

import { CardSearchService } from '../card-search/card-search.service';

@Component({
  selector: 'app-warning-message',
  templateUrl: './warning-message.component.html',
  styleUrls: ['./warning-message.component.css']
})
export class WarningMessageComponent {
  @Input() warning: string;

  constructor(private cardsearchservice: CardSearchService) {
    this.cardsearchservice.emitWarning.subscribe((warning: string) => this.warning = warning);
  }

  updateWarning(msg: string) {
    console.log(msg);
    this.warning = msg;
  }
}
