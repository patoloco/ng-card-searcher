import { Routes } from '@angular/router';

import { CardDetailComponent } from './card-detail/card-detail.component';
import { CardSearchComponent } from './card-search/card-search.component';
import { CardTextSearchComponent } from './card-search/card-text-search.component';
import { TabManaColorsComponent } from './card-detail/tab-mana-colors/tab-mana-colors.component';
import { TabTextComponent } from './card-detail/tab-text/tab-text.component';
import { TabTypesComponent } from './card-detail/tab-types/tab-types.component';

export const routes: Routes = [
  { path: '', component: CardSearchComponent, pathMatch: 'full' },
  { path: 'byname', component: CardSearchComponent, pathMatch: 'full' },
  { path: 'byname/:name', component: CardDetailComponent, children: [
    { path: '', component: TabTypesComponent },
    { path: 'tab-types', component: TabTypesComponent },
    { path: 'tab-mana-colors', component: TabManaColorsComponent },
    { path: 'tab-text', component: TabTextComponent }
  ] },
  { path: 'bytext', component: CardTextSearchComponent, pathMatch: 'full' }
];
