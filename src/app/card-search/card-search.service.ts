import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';

import { Card } from '../Card';

import { devUrl, url } from '../urlList';

const emptyCard: Card = {
  cmc: 0,
  colorIdentity: [],
  colors: [],
  imageName: '',
  layout: '',
  manaCost: '',
  name: '',
  power: '',
  subtypes: [],
  text: '',
  toughness: '',
  type: '',
  types: []
};

@Injectable()
export class CardSearchService {
  emitWarning = new EventEmitter<string>();
  cardUpdated = new EventEmitter<Card>();
  private cardUrl = url.card;
  private devCardUrl = devUrl.card;
  cardOne: Card = emptyCard;

  constructor(private http: Http) {}

  getCard(name: string): void {
    const request = this.createCORSRequest('get', this.cardUrl.concat(name));
    if (request) {
      request.onload = () => {
        if (request.responseText) {
          this.cardOne = JSON.parse(request.responseText) as Card;
          this.cardUpdated.emit(this.cardOne);
          this.emitWarning.emit('');
        } else {
          this.emitWarning.emit('Invalid Search');
        }
      };
      request.send();
    }
  }

  getCurrentCard(): Card {
    return this.cardOne;
  }

  clearCurrentCard(): void {
    this.cardOne = emptyCard;
  }

  createCORSRequest(method: string, url: string) {
    let xhr = new XMLHttpRequest();

    if ('withCredentials' in xhr) {
      xhr.open(method, url, true);
    } else {
      xhr = null;
    }
    return xhr;
  }

}
