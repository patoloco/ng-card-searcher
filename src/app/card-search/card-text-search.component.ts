import { Component, OnInit } from '@angular/core';
import { Angular2Csv } from 'angular2-csv';
import { Subject } from 'rxjs/Subject';

import { CardSearchService } from '../card-search/card-search.service';
import { CardTextSearchService } from '../card-search/card-text-search.service'

@Component({
  selector: 'app-card-text-search',
  templateUrl: './card-text-search.component.html',
  styleUrls: ['./card-search.component.css']
})

export class CardTextSearchComponent implements OnInit {
  model = { text: '' }
  p: number;

  results: string[] = [''];
  searchTerm$ = new Subject<string>();

  constructor(private cardtextsearchservice: CardTextSearchService,
            private cardsearchservice: CardSearchService) {

    this.cardtextsearchservice.search(this.searchTerm$)
      .subscribe(results => {
          this.results = results || [''];
    });
  }

  getCard(name: string): void {
    this.cardsearchservice.getCard(name);
  }

  ngOnInit() {

  }

  toCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: false,
      showTitle: true,
      useBom: true,
      headers: Object.keys(this.results[0]),
      title: 'Results of Search'
    };

    const csv = new Angular2Csv(this.results, 'Results', options);
  }
}
